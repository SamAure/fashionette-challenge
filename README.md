# Fashionette Challenge

## Requierements 

* PHP 7.4

## Setup steps

1. Run 'composer install'
2. Run 'cp .env-example .env', and add valid DB credentials
3. Run 'php artisan migrate'
4. Run 'php -S localhost:8080 -t public' to serve the app on 'localhost:8080'

*Note: All commands must be run on root folder of the project*

## Tests

There are some tests in ./tests

## Recommendations for this api

Please see the file ./notes.txt
