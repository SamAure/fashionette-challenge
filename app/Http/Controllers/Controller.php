<?php

namespace App\Http\Controllers;

use App\Integrations\TVMaze\TVMazeService;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    private TVMazeService $tv_maze_service;

    public function __construct(TVMazeService $tv_maze_service)
    {
        $this->tv_maze_service = $tv_maze_service;
    }

    public function searchShow(Request $r)
    {

        $has_term = $r->has('q');

        if (!$has_term)
            return response(['error' => 'You must specify the search term in get parameter <q>'], 400);

        $query = $r->get('q');
        $lowercase_query = strtolower($query);

        $shows_results = $this->tv_maze_service->searchShows($query);

        if (empty($shows_results)) return response($shows_results);

        $allowed_shows = [];

        foreach ($shows_results as $show_result) {
            $show = $show_result->show;
            if (strtolower($show->name) === $lowercase_query)
                $allowed_shows[] = $show;
        }

        return response($allowed_shows);
    }
}
