<?php

namespace App\Integrations\TVMaze\DTO;

class Country
{
    public string $name;
    public string $code;
    public string $timezone;
}
