<?php

namespace App\Integrations\TVMaze\DTO;

class Externals
{
    public ?int $tvrage;
    public ?int $thetvdb;
    public ?string $imdb;
}
