<?php

namespace App\Integrations\TVMaze\DTO;

class Image
{
    public string $medium;
    public string $original;
}
