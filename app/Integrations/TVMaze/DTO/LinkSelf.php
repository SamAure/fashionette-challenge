<?php

namespace App\Integrations\TVMaze\DTO;

class LinkSelf
{
    public string $href;
}
