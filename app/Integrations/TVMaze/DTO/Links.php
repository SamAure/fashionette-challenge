<?php

namespace App\Integrations\TVMaze\DTO;

class Links
{
    public LinkSelf $self;
    public PreviousEpisode $previousepisode;
}
