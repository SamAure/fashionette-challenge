<?php

namespace App\Integrations\TVMaze\DTO;

class Network
{
    public int $id;
    public string $name;
    public Country $country;
}
