<?php

namespace App\Integrations\TVMaze\DTO;

class PreviousEpisode
{
    public string $href;
}
