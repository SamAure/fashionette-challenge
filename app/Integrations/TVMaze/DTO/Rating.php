<?php

namespace App\Integrations\TVMaze\DTO;

class Rating
{
    public ?float $average;
}
