<?php

namespace App\Integrations\TVMaze\DTO;

class Schedule
{
    public string $time;
    public array $days;  //array<string>
}
