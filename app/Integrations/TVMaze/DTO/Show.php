<?php

namespace App\Integrations\TVMaze\DTO;

use DateTime;

class Show
{
    public int $id;
    public string $url;
    public string $name;
    public string $type;
    public ?string $language;
    public array $genres;  //array<string>
    public string $status;
    public ?int $runtime;
    public ?int $averageRuntime;
    public ?DateTime $premiered;
    public ?DateTime $ended;
    public ?string $officialSite;
    public Schedule $schedule;
    public Rating $rating;
    public int $weight;
    public ?Network $network;
    public ?array $webChannel; //array<string>
    public ?array $dvdCountry; //array<string>
    public Externals $externals;
    public ?Image $image;
    public ?string $summary;
    public int $updated;
    public Links $_links;
}
