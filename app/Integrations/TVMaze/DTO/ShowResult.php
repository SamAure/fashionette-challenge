<?php

namespace App\Integrations\TVMaze\DTO;

class ShowResult
{
    public float $score;
    public Show $show;
}
