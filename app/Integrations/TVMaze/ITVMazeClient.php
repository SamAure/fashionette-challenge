<?php

namespace App\Integrations\TVMaze;

interface ITVMazeClient
{
    public function searchShows(string $query): string;
}
