<?php

namespace App\Integrations\TVMaze;

use App\Models\CachedResult;
use Carbon\Carbon;

class TVMazeCacheManager
{
    public function fetchData(string $query): ?string
    {
        $cached_result = CachedResult::where('query', $query)->orderBy('created_at', 'desc')->first();
        if (is_null($cached_result)) return null;

        $now = Carbon::now();
        $diff_in_seconds = $now->diff($cached_result->created_at)->format('%s');

        if($diff_in_seconds > 600) return null;

        return $cached_result->data;
    }

    public function saveData(string $query, string $data)
    {
        $cached_result = new CachedResult(['query' => $query, 'data' => $data,]);
        $cached_result->save();
    }
}
