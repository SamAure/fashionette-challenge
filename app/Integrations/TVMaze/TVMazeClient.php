<?php

namespace App\Integrations\TVMaze;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class TVMazeClient implements ITVMazeClient
{
    private Client $http_client;
    private const BASE_URL = 'https://api.tvmaze.com';

    public function __construct(Client $http_client)
    {
        $this->http_client = $http_client;
    }

    public function searchShows(string $query): string
    {
        $url = self::BASE_URL . '/search/shows?q=';

        try {

            $res = $this->http_client->request('GET', $url, [
                'query' => ['q' => $query],
            ]);

            return $res->getBody();

        } catch(GuzzleException $e) {
            // Log error or something
            return '[]';
        }
    }

}
