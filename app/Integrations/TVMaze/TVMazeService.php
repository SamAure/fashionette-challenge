<?php

namespace App\Integrations\TVMaze;

use App\Integrations\TVMaze\DTO\ShowResult;
use JsonMapper;

class TVMazeService
{

    private ITVMazeClient $client;
    private JsonMapper $mapper;
    private TVMazeCacheManager $cache_manager;

    public function __construct(ITVMazeClient $client, JsonMapper $mapper, TVMazeCacheManager $cache_manager)
    {
        $this->client = $client;
        $this->mapper = $mapper;
        $this->cache_manager = $cache_manager;
    }

    /**
     * @param string $query
     * @return ShowResult[]
     */
    public function searchShows(string $query): array
    {
        $cached_data = $this->cache_manager->fetchData($query);

        if(is_null($cached_data)) {
            $result_json = $this->client->searchShows($query);

            $this->cache_manager->saveData($query, $result_json);
        } else {
            $result_json = $cached_data;
        }

        $result = json_decode($result_json);

        $shows = $this->mapper->mapArray(
            $result, [], ShowResult::class
        );

        return $shows;
    }
}
