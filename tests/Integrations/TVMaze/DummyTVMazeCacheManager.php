<?php

namespace Integrations\TVMaze;

class DummyTVMazeCacheManager extends \App\Integrations\TVMaze\TVMazeCacheManager
{
    public array $cached_data = [];
    public bool $should_return = false;
    public int $cache_returned_counter = 0;

    public function fetchData(string $query): ?string
    {
        if (isset($this->cached_data[$query]) && $this->should_return) {
            $this->cache_returned_counter++;
            return $this->cached_data[$query];
        }
        return null;
    }

    public function saveData(string $query, string $data)
    {
        $this->cached_data[$query] = $data;
    }
}
