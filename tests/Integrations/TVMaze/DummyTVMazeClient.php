<?php

namespace Integrations\TVMaze;

use App\Integrations\TVMaze\ITVMazeClient;

class DummyTVMazeClient implements ITVMazeClient
{
    public string $data_to_return;
    public function searchShows(string $query): string
    {
        return $this->data_to_return;
    }
}
