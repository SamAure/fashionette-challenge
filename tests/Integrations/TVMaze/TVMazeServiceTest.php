<?php

namespace Integrations\TVMaze;

use App\Integrations\TVMaze\DTO\Show;
use App\Integrations\TVMaze\DTO\ShowResult;
use App\Integrations\TVMaze\TVMazeService;
use JsonMapper;
use PHPUnit\Framework\TestCase;
use function PHPUnit\Framework\assertNotNull;

class TVMazeServiceTest extends TestCase
{

    public function testSearchShows()
    {
        $dummy_cache_manager = new DummyTVMazeCacheManager();
        $dummy_client = new DummyTVMazeClient();
        $json_mapper = new JsonMapper();

        $service = new TVMazeService($dummy_client, $json_mapper, $dummy_cache_manager);

        assertNotNull($service);

        $query = 'something';
        $dummy_client->data_to_return = '[]';
        $dummy_cache_manager->should_return = true;

        $show_results = $service->searchShows($query);
        self::assertEmpty($show_results);
        self::assertEquals($dummy_cache_manager->cache_returned_counter, 0);

        $show_results = $service->searchShows($query);
        self::assertEmpty($show_results);
        self::assertEquals($dummy_cache_manager->cache_returned_counter, 1);

        $dummy_cache_manager->should_return = false;
        $show_results = $service->searchShows($query);
        self::assertEmpty($show_results);
        self::assertEquals($dummy_cache_manager->cache_returned_counter, 1);

        $raw_data = file_get_contents(__DIR__.'/raw_response.json');
        $dummy_client->data_to_return = $raw_data;
        $show_results = $service->searchShows($query);
        self::assertNotEmpty($show_results);
        self::assertEquals(ShowResult::class, get_class($show_results[0]));
        self::assertEquals(Show::class, get_class($show_results[0]->show));
        self::assertEquals(161, $show_results[0]->show->id);


        // Add a manual creation of the expected DTOs and validate this against the output of the service


    }
}
